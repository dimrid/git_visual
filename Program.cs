﻿using System;



namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {



                Console.WriteLine("Чтобы очистить калькулятор намите Enter.");

                ConsoleKey key = Console.ReadKey().Key;
                if (key == ConsoleKey.Enter)
                {
                    Console.Clear();
                }

                double firstValue, secondValue;
                string action;

                try
                {
                    Console.WriteLine("Введите первое число.");
                    firstValue = double.Parse(Console.ReadLine());

                    Console.WriteLine("Введите второе число.");
                    secondValue = double.Parse(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("Ошибка неверное число.");
                    Console.ReadLine();
                    continue;
                }
                Console.WriteLine("Введите знак действия.");
                action = Console.ReadLine();

                switch (action)
                {
                    case "+":
                        Console.WriteLine("Ответ: " + firstValue + secondValue);
                        break;

                    case "-":
                        Console.WriteLine($"Ответ: {firstValue - secondValue}");
                        break;

                    case "*":
                        Console.WriteLine("Ответ: " + firstValue * secondValue);
                        break;

                    case "/":
                        Console.WriteLine("Ответ: " + firstValue / secondValue);
                        break;

                    default:
                        Console.WriteLine("Неверный знак действия.");
                        break;

                        Console.ReadLine();

                }

            }

        }
    }
}